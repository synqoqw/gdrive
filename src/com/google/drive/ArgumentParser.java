// Copyright 2012 Google Inc. All Rights Reserved.

package com.google.drive;



import java.util.Hashtable;

/**
 * Argument Parser
 * 
 * Parses the command line arguments and provides an easy way to access and fetch them
 * 
 * @author Gianpaolo Surfaro <gianpaolos@google.com>
 * @version 1.0, 13/11/2012
 */
class ArgumentParser {

  /**
   * The hash table containing the pairs flag-value
   */
  private final Hashtable<String, String> options = new Hashtable<String, String>();

  /**
   * Parses the arguments from the string array and puts them in a hash table
   * 
   * @param args the command line arguments
   */
  public ArgumentParser(String[] args) {
    for (String arg : args) {
      if (arg.startsWith("-")) {
        int loc = arg.indexOf("=");
        String key = (loc > 0) ? arg.substring(1, loc) : arg.substring(1);
        String value = (loc > 0) ? arg.substring(loc + 1) : "";
        options.put(key.toLowerCase(), value);
      } else {
        System.out.println(String.format("[WARNING] Cannot parse argument %s", arg));
      }
    }
  }

  /**
   * Checks if an option/flag is present
   * 
   * @param opt the flag to check
   * @return true if it's present, false otherwise
   */
  public boolean hasOption(String opt) {
    return options.containsKey(opt.toLowerCase());
  }

  /**
   * Gets the value of a option/flag
   * 
   * @param opt the option/flag to fetch the value for
   * @return the value of the flag/option
   */
  public String getOption(String opt) {
    return options.get(opt.toLowerCase());
  }

}
