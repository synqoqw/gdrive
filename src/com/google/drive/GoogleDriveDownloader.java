package com.google.drive;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.Drive.Children;
import com.google.api.services.drive.model.ChildList;
import com.google.api.services.drive.model.ChildReference;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class GoogleDriveDownloader {

  /**
   * Instance of google drive.
   */
  private static Drive gdriveApiClient;

  /**
   * Get authorized google drive api client
   * 
   * @return gdriveApiClient the authorized API client
   */
  private Drive getGDriveApiClient(String serviceAccountEmail, String privateKey) {
    HttpTransport httpTransport = new NetHttpTransport();
    JacksonFactory jsonFactory = new JacksonFactory();
    try {
      GoogleCredential credential =
          new GoogleAuthenticator(httpTransport, serviceAccountEmail, privateKey).getCredential();
      gdriveApiClient =
          new Drive.Builder(httpTransport, jsonFactory, null).setHttpRequestInitializer(credential)
              .build();

    } catch (Exception e) {
      System.out.println(String.format("Google drive authentication failed! : %s", e.getMessage()));
      System.exit(-1);
    }

    return gdriveApiClient;
  }

  /**
   * Download a file's content.
   * 
   * @param gdriveApiClient Drive API service instance.
   * @param file Drive File instance.
   * @return InputStream containing the file's content if successful, {@code null} otherwise.
   */

  private static InputStream downloadFile(Drive gdriveApiClient, File file) {
    if (file.getDownloadUrl() != null && file.getDownloadUrl().length() > 0) {
      try {
        HttpResponse resp =
            gdriveApiClient.getRequestFactory()
                .buildGetRequest(new GenericUrl(file.getDownloadUrl())).execute();
        return resp.getContent();
      } catch (Exception e) {
        e.printStackTrace();
        return null;
      }
    } else {
      // The file doesn't have any content stored on Drive.
      return null;
    }
  }

  /**
   * Save File locally
   * 
   * @param Hashmap with file details
   * @param String download folder path
   */
  private void saveFile(HashMap<Date, String> dateFileIdMap, String downloadFolder) {
    List<Date> dates = new ArrayList<Date>(dateFileIdMap.keySet());
    String fileId = dateFileIdMap.get(Collections.max(dates));
    File file;
    try {
      file = gdriveApiClient.files().get(fileId).execute();
      String buildNo = file.getOriginalFilename();
      System.out.println(String.format("Downloading %s.Created on %s...", buildNo,
          file.getCreatedDate()));
      String remoteMD5 = file.getMd5Checksum().toString();
      String loaclFilePath = String.format("%s/%s", downloadFolder, buildNo);
      java.io.File loaclBuild = new java.io.File(loaclFilePath);
      if (loaclBuild.exists()) {
        FileInputStream fis = new FileInputStream(new java.io.File(loaclFilePath));
        String localMD5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);
        if (localMD5.equals(remoteMD5)) {
          System.out.println("Build exists locally.Skipping the download.");
          System.exit(-1);
        }
      }
      InputStream gdiveInStr = downloadFile(gdriveApiClient, file);
      OutputStream outputStream = null;
      outputStream = new FileOutputStream(new java.io.File(loaclFilePath));
      int read = 0;
      byte[] bytes = new byte[1024];
      while ((read = gdiveInStr.read(bytes)) != -1) {
        outputStream.write(bytes, 0, read);
      }
      outputStream.close();
      FileInputStream fis = new FileInputStream(new java.io.File(loaclFilePath));
      String localMD5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);
      if (localMD5.equals(remoteMD5)) {
        System.out.println("Download complete.");
      } else {
        System.out.println(String.format("Error while downloading build %s : ", buildNo));
      }
    } catch (Exception e) {
      System.out.println("Exception while saving the file : " + e.getMessage());
      System.exit(-1);
    }

  }

  /**
   * Search file in google drive.
   * 
   * @param String driveFolderId
   * @param String filePrefix
   */

  private HashMap<Date, String> searchFile(String driveFolderId, String filePrefix) {
    HashMap<Date, String> dateFileIdMap = new HashMap<Date, String>();
    Children.List request;
    try {
      System.out.println(String.format("Searching the build with prefix %s ", filePrefix));
      request = gdriveApiClient.children().list(driveFolderId).setQ("trashed = false");
      ChildList children = request.execute();
      DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
      for (ChildReference child : children.getItems()) {
        File file = gdriveApiClient.files().get(child.getId()).execute();
        switch (filePrefix.toLowerCase()) {
          case "core_saver_docx_test":
            if (file.getTitle().toLowerCase().matches("core_saver_docx_test_v\\d{1,10}.zip")) {
              Date d = df.parse(file.getCreatedDate().toString());
              dateFileIdMap.put(d, file.getId());
            } else {
              continue;
            }
            break;
          case "cmd_line":
            if (file.getTitle().toLowerCase().matches("cmd_line_v\\d{1,10}.zip")) {
              Date d = df.parse(file.getCreatedDate().toString());
              dateFileIdMap.put(d, file.getId());
            } else {
              continue;
            }
            break;
          default:
            if (!(file.getTitle().contains("-"))) {
              continue;
            }
            int indexOfLastHyphen = file.getTitle().lastIndexOf("-");
            String build = file.getTitle().substring(indexOfLastHyphen + 1);
            if (!(build.contains("."))) {
              continue;
            }
            String[] prefixArr = build.split("\\.");
            String prefixString =
                String.format("%s.%s.%s", prefixArr[0], prefixArr[1], prefixArr[2]);
            if (prefixString.equalsIgnoreCase(filePrefix)) {
              Date d = df.parse(file.getCreatedDate().toString());
              dateFileIdMap.put(d, file.getId());
            }
            break;
        }

      }
    } catch (Exception e) {
      System.out.println(String.format("Error while searching %s", e.getMessage()));
      System.exit(-1);
    }
    return dateFileIdMap;
  }

  public static void main(String args[]) {
    GoogleDriveDownloader gd = new GoogleDriveDownloader();
    ArgumentParser argumentParser = new ArgumentParser(args);
    if (!argumentParser.hasOption("service_account_email")
        || !argumentParser.hasOption("drive_folder_id") || !argumentParser.hasOption("file_prefix")
        || !argumentParser.hasOption("download_folder") || !argumentParser.hasOption("private_key")) {
      System.out
          .println("Usage: java -jar <jar> -service_account_email=<service account email> -drive_folder_id=<google drive folder id> -file_prefix=<five character string ex(0.0.0)> -download_folder=<download folder path> -private_key=<private key path>");
      if (!argumentParser.hasOption("service_account_email")) {
        System.out.println("service_account_email flag is required.");
      }
      if (!argumentParser.hasOption("drive_folder_id")) {
        System.out.println("drive_folder_id flag is required.");
      }
      if (!argumentParser.hasOption("file_prefix")) {
        System.out.println("file_prefix flag is required.");
      }
      if (!argumentParser.hasOption("download_folder")) {
        System.out.println("download_folder flag is required.");
      }
      if (!argumentParser.hasOption("private_key")) {
        System.out.println("private_key flag is required.");
      }
      System.exit(-1);
    }
    String serviceAccountEmail =
        argumentParser.hasOption("service_account_email") ? argumentParser
            .getOption("service_account_email") : null;
    String driveFolderId =
        argumentParser.hasOption("drive_folder_id")
            ? argumentParser.getOption("drive_folder_id")
            : null;
    String filePrefix =
        argumentParser.hasOption("file_prefix") ? argumentParser.getOption("file_prefix") : null;
    String downloadFolder =
        argumentParser.hasOption("download_folder")
            ? argumentParser.getOption("download_folder")
            : null;
    String privateKey =
        argumentParser.hasOption("private_key") ? argumentParser.getOption("private_key") : null;

    gdriveApiClient = gd.getGDriveApiClient(serviceAccountEmail, privateKey);
    HashMap<Date, String> dateFileIdMap = gd.searchFile(driveFolderId, filePrefix);
    if (dateFileIdMap.isEmpty()) {
      System.out.println("Build with provided prefix does not exist on google drive : "
          + filePrefix);
      System.exit(-1);
    }
    gd.saveFile(dateFileIdMap, downloadFolder);
  }
}
