package com.google.drive;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.DriveScopes;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collection;


public class GoogleAuthenticator {

  /**
   * JSON factory used in the credentials generation.
   */
  private static final JsonFactory JSON_FACTORY = new JacksonFactory();

  /**
   * Instance of google credentials that will be used to authenticate.
   */
  private final GoogleCredential credential;

  /**
   * Initialises a default HTTP transport.
   * 
   * @param scopes the required scopes
   */
  public GoogleAuthenticator(Collection<String> scopes) throws GeneralSecurityException,
      IOException {
    this(new NetHttpTransport(), new java.lang.String(), new java.lang.String());
  }

  /**
   * Initialises the credentials for the account linked to the passed key.p12.
   * 
   * @param scopes the required scopes.
   * @param httpTransport the HTTP transport instance.
   */
  @SuppressWarnings("deprecation")
  public GoogleAuthenticator(HttpTransport httpTransport, String email, String privateKey)
      throws GeneralSecurityException, IOException {
    // Build service account credential.
    this.credential =
        new GoogleCredential.Builder().setTransport(httpTransport).setJsonFactory(JSON_FACTORY)
            .setServiceAccountId(email).setServiceAccountScopes(DriveScopes.DRIVE)
            .setServiceAccountPrivateKeyFromP12File(new java.io.File(privateKey)).build();
  }

  /**
   * @return the Google credential instance.
   */
  public GoogleCredential getCredential() {
    return credential;
  }
}
